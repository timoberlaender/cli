package de.grogra.cli.completer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.jline.reader.impl.completer.ArgumentCompleter;
import org.jline.reader.impl.completer.NullCompleter;
import de.grogra.cli.CLIApplication;
import de.grogra.cli.Utils;

public class ApplicationOnFileCompleter extends ArgumentCompleter{

	public ApplicationOnFileCompleter(CLIApplication app) {
		
		List<String> cmdAppWithFilesArgs = new ArrayList<String>();
		List<String> cmdApp = Utils.getAppCommands(app);
		//remove open, close & select
		for (String s : cmdApp) {
			if (s.endsWith("open") || s.endsWith("cd") || s.endsWith("view")) {
				cmdAppWithFilesArgs.add(s);
			}
		}
		cmdApp.removeAll(cmdAppWithFilesArgs);
		
		RegistryItemCompleter commandsWithFileArgs = new RegistryItemCompleter(cmdAppWithFilesArgs);
		
		this.getCompleters().addAll(Arrays.asList(commandsWithFileArgs, app.getSysFileCompleter(), NullCompleter.INSTANCE));
		
	}
}
