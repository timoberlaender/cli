package de.grogra.cli.completer;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.function.Supplier;

import org.jline.builtins.Completers.FileNameCompleter;

public class SystemFileCompleter extends FileNameCompleter{
	
	private Supplier<Path> currentDir;

    public SystemFileCompleter(File currentDir) {
        this(currentDir.toPath());
    }

    public SystemFileCompleter(Path currentDir) {
        this.currentDir = () -> currentDir;
    }

    public SystemFileCompleter(Supplier<Path> currentDir) {
        this.currentDir = currentDir;
    }
    
    public void changeDir(File currentDir) {
    	changeDir(currentDir.toPath());
    }
    
    public void changeDir(Path currentDir) {
    	changeDir(() -> currentDir);
    }
    
    public void changeDir(Supplier<Path> currentDir) {
        this.currentDir = currentDir;
    }

    @Override
    protected Path getUserDir() {
        return currentDir.get();
    }

}
