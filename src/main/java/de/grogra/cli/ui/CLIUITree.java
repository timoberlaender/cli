package de.grogra.cli.ui;

import javax.swing.tree.TreeModel;
import de.grogra.pf.registry.Item;
import de.grogra.pf.ui.Command;
import de.grogra.pf.ui.Showable;
import de.grogra.pf.ui.Workbench;
import de.grogra.pf.ui.awt.AWTSynchronizer;
import de.grogra.pf.ui.tree.SyncMappedTree;
import de.grogra.pf.ui.tree.UITree;
import de.grogra.pf.ui.Panel;
import de.grogra.pf.ui.util.ComponentWrapperImpl;
import de.grogra.util.Described;
import de.grogra.cli.ExecutableComponent;
import de.grogra.graph.impl.Node;
import de.grogra.imp.ObjectInspector.TreeNode;


public class CLIUITree extends CLIComponent {
	
	int iterable = 0;
	
	@Override
	public void dispose() {
		((SyncMappedTree)getContent()).dispose();
	}

	@Override
	public CLIComponent getComponent() {
		return this;
	}
	
	public CLIUITree(UITree tree) {
		setName("uitree");
		setShowable(true);
		setContent(new SyncMappedTree (tree, tree.getRoot (), new AWTSynchronizer (null)));
	}
	
	public void show() {
		TreeModel model = getSourceTree();
		iterable=0;
		ComponentWrapperImpl c = new ComponentWrapperImpl(getTreeText(model, model.getRoot(), ""), null);
		try {
			((CLIWindowSupport)getSupport().getWindow()).consoleWrite(c);
		}
		catch (NullPointerException e)
		{
			System.out.println("Cannot display the panel because it doesn't have a support");
		}
	}
	
	private String getTreeText(TreeModel model, Object object, String indent) {
	    String myRow = indent + "[" + iterable + "] : " + getNodeText(model, object )
	    	+ System.lineSeparator();
	    iterable++;
	    for (int i = 0; i < model.getChildCount(object); i++) {
	        myRow += getTreeText(model, model.getChild(object, i), indent + "  ");
	    }
	    return myRow;
	}
	
	public String getTreeText() {
		TreeModel model = getSourceTree();
		return getTreeText(model, model.getRoot(), "");
	}
	
	
	public TreeModel getSourceTree ()
	{
		return ((SyncMappedTree)getContent()).getSourceTree();
	}
	
	private String getNodeText(TreeModel model, Object item) {
		return String.valueOf (((UITree) model).getDescription
				   (item, Described.NAME));
	}
	
	/**
	 * Get a node from the tree matching the given id. It first look for name, then id #.
	 * @param id the name (String), or id (int) of the node to retrieve
	 * @return the node that fit the requirement or null
	 */
	private Object getNode(String id) {
		TreeModel model = getSourceTree();
		// check if a name fit
		Object node = getNodeFromName(model, model.getRoot(), id);
		if (node!=null)
			return node;
		try {
			int index = Integer.valueOf(id);
			iterable=0;
			return getNodeFromId(model, model.getRoot(), index);
		}catch (NumberFormatException e) {
			getSupport().getWorkbench().logInfo("Cannot select object: either name not fit or id out of bound");
		}
		return null;
		
	}
	
	private Object getNodeFromName(TreeModel model, Object node, String name) {
		Object n=null;
		if (name.equals(getNodeText(model, node))) {
			n=node;
			return n;
		}
		for (int i = 0; i < model.getChildCount(node); i++) {
			n=getNodeFromName(model, model.getChild(node, i), name );
			if (n!=null) break;
	    }
		return n;
	}
		
	/**
	 * return the Nth node of the treemodel. 
	 */
	private Object getNodeFromId(TreeModel model, Object node, int index) {
		Object n=null;
		if (iterable == index) {
			n=node;
			return n;
		}
		iterable++;
		for (int i = 0; i < model.getChildCount(node); i++) {
			n=getNodeFromId(model, model.getChild(node, i), index );
			if (n!=null) break;
	    }
		return n;
	}
	
//	private Object addObject(String newName) {
//		// check if tree editable
//		// check if param is correct string (with extension)
//		// use "object>new from menu"
//		UITree menu = getSupport().getMenu();
//		Object parentNode = getNodeFromName(menu, menu.getRoot(), "New");
//		if (parentNode!=null && !menu.isLeaf(parentNode) ) {
//			try {
//			Object addNode = menu.getChild(parentNode, 0);
//			ItemFactory factory = (ItemFactory) ((UITreePipeline.Node)addNode).getNode();
//			Item i = factory.createItem(getSupport().getWorkbench());
//			
//			
//			}
//			catch(Exception e) {
//				getSupport().getWorkbench().logInfo("Factory for explorer object not found.");
//			}
//		}
//		
//
//		return null;
//	}
//	
	
	/**
	 * Select the treenode in the workbench. Return true if the treeNode could be selected
	 * @param treeNode
	 * @return
	 */
	private boolean selectNode(Object treeNode) {
		if (treeNode !=null && treeNode instanceof TreeNode) {
			Object n = ((TreeNode)treeNode).getObject();
			if (n != null && n instanceof Node) {
				Node[] nodes = new Node[] {(Node) n};
				getSupport().getWorkbench().select(nodes);
				return true;
			}
		}
		return false;
	}

	
	/**
	 * The possible executable runs are (displayed as: runName (Param1, param2, ..):
	 * 
	 * 1) show (String id): open a "showable" object (e.g. .rgg, .java files) into the text editor.
	 * The parameter id is either the name of the object (e.g. Model.rgg) or its number in the list.
	 * (usually 0 is the root of the tree. e.g. for the example model project, %fileexplorer:show 1 
	 * open nano on Model.rgg.
	 * 
	 * 2) add (String name): add an object of the tree type (the explorer resource directory associated
	 * with the tree). name is the name of the object, if no extension is given (e.g. name="Model") 
	 * the method try to find an appropriate extension (not so efficient so you should given the 
	 * extension in the name).
	 * 
	 * 3) delete (String id): remove an object from the project. The object is selected by name or id
	 * the same way as show pick object from the tree.
	 * 
	 * 4) rename (String id, String newName): rename an object from the tree. The object selected by the
	 * id the same way as show. newName is the new name of the object (extension included).
	 */
	@Override
	public void run(String s, Object info) {
		super.run(s,info);
		switch (s) {
		case("show"):
			try {
			String param = de.grogra.cli.Utils.getStringParameter(info,false); 
			Object node = getNode(param);
			// either the param is a string (systemID of an object to view)
			if (node instanceof Showable) {
				((Showable)node).show(Workbench.current());
			} else if(selectNode(node)) {
				((Command)Item.resolveItem(getSupport(),"/ui/panels/attributeeditor")).run(null, getSupport());
				Panel p = getSupport().getWindow().getPanel("/ui/panels/attributeeditor");
				((ExecutableComponent)p).run("repaint", null);
			}
			else {
				getSupport().getWorkbench().logInfo("Node not showable.");
			}
			}catch(NullPointerException e) {
				getSupport().getWorkbench().logInfo("No value given.");
			}
			break;
		case("select"):
			try {
			String p2 = de.grogra.cli.Utils.getStringParameter(info,false); 
			Object n2 = getNode(p2);
			selectNode(n2);
			}catch(NullPointerException e) {
				getSupport().getWorkbench().logInfo("No value given.");
			}
			break;
		}
	}
}
