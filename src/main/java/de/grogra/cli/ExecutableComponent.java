package de.grogra.cli;

public interface ExecutableComponent {

	/**
	 * An interface to define ComponentWrapper whose component can be run.
	 */
	
	void run(String s, Object info);
}
